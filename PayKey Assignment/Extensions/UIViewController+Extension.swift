//
//  UIViewController+Extension.swift
//  PayKey Assignment
//
//  Created by Itzik Bar-Noy on 09/11/2019.
//  Copyright © 2019 Itzik Bar-Noy. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    func showGenericError() {
        let alert = UIAlertController(title: Constants.ErrorMessage.title, message: Constants.ErrorMessage.description, preferredStyle: .alert)
        let action = UIAlertAction(title: "O.K.", style: .destructive)
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}
